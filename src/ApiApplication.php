<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase;

use Silex\Provider\TwigServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use SilexBase\Provider\JMSSerializerServiceProvider;
use SilexBase\Provider\JsonRequestServiceProvider;

/**
 * Base application of an API
 *
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase
 */
class ApiApplication extends Application
{
    /**
     * {@inheritDoc}
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->init('Serializer');
        $this->init('WebProfiler');
        $this->init('JsonRequests');
    }

    /**
     * Initialise JMS serializer services
     *
     * @return bool
     */
    protected function initSerializer()
    {
        $app = $this;

        $app->register(
            new JMSSerializerServiceProvider(),
            array(
                'serializer.cache_dir' => $app['config']['cache']['cache_dir'] . '/serializer',
                'serializer.metadata' =>
                    isset($app['config']['serializer']['metadata']) ? $app['config']['serializer']['metadata'] : array()
            )
        );

        return true;
    }


    /**
     * Initialise debug web profiler
     *
     * @return bool
     */
    protected function initWebProfiler()
    {
        $app = $this;

        if (!$app['debug']) {
            return true;
        }

        $app->init('Controllers');


        $app->register(
            new TwigServiceProvider(),
            array(
                'twig.path'           =>
                    isset($app['config']['views']['paths']) ? $app['config']['views']['paths'] : array(),
                'twig.options'        => array(),
                'twig.form.templates' => array(),
            )
        );

        $app->register(
            new WebProfilerServiceProvider(),
            array(
                'profiler.cache_dir'    => $app['config']['cache']['cache_dir'] . '/profiler',
                'profiler.mount_prefix' => '/_profiler',
                'web_profiler.debug_toolbar.enable' => false
            )
        );

        if (class_exists('Saxulum\SaxulumWebProfiler\Provider\SaxulumWebProfilerProvider')) {
            $app->register(new \Saxulum\SaxulumWebProfiler\Provider\SaxulumWebProfilerProvider());
        }

        return true;
    }

    /**
     * Handle JSON request's
     *
     * @return bool
     */
    protected function initJsonRequests()
    {
        $app = $this;

        $app->register(new JsonRequestServiceProvider());
    }
}
