<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Tests;

use Silex\WebTestCase as BaseWebTestCase;
use SilexBase\Application;

/**
 * Class WebTestCase
 *
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Tests
 */
abstract class WebTestCase extends BaseWebTestCase
{
    /**
     * @return Application
     */
    public function createApplication()
    {
        $app = new Application(
            array(
                'debug' => false,
                'env' => 'test'
            )
        );

        $app['exception_handler']->disable();
        $app['session.test'] = true;

        return $app;
    }
}
