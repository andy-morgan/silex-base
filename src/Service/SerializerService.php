<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Service;

use JMS\Serializer\Serializer as JmsSerializer;
use JMS\Serializer\SerializationContext;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Service
 */
class SerializerService
{
    /** @var JmsSerializer  */
    protected $serializer;

    /**
     * @param JmsSerializer $serializer
     */
    public function __construct(JmsSerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param mixed $data
     * @param array|string $groups
     * @return string
     */
    public function toJson($data, $groups = 'Default')
    {
        $json = $this->serializer->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups($groups)
        );

        return $json;
    }
}
