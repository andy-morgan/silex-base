<?php
/**
 * @copyright   Copyright (c) 2015 BoxUK Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * @package SilexBase\EventListener
 */
class ApiExceptionListener
{

    /** @var bool */
    protected $debug;

    /**
     * @param bool $debug
     */
    public function __construct($debug = false)
    {
        $this->debug = $debug;
    }

    /**
     * Handles security related exceptions.
     *
     * @param GetResponseForExceptionEvent $event An GetResponseForExceptionEvent instance
     */
    public function onCoreException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if (!$exception instanceof HttpException) {
            return;
        }

        $data = [];
        if (strlen($exception->getMessage())) {
            $data['message'] = $exception->getMessage();
        }
        if ($this->debug) {
            $data['file'] = $exception->getFile();
            $data['line'] = $exception->getLine();
            $data['trace'] = $exception->getTrace();
        }

        $response = new JsonResponse($data);
        $response->setStatusCode($exception->getStatusCode());

        $event->setResponse($response);
    }
}
