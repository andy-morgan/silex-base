<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase;

/**
 * Base application of a web site with a GUI
 *
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase
 */
class WebApplication extends Application
{
    /**
     * {@inheritDoc}
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->init('Twig');
        $this->init('WebProfiler');
        $this->init('Assetic');
        $this->init('HttpCache');
        $this->init('Sessions');
        $this->init('Bootstrap');
    }

    /**
     * Initialise twig for template rendering
     *
     * @return bool
     */
    protected function initTwig()
    {
        $app = $this;

        if (method_exists($this, 'initSecurity')) {
            $this->init('Security');
        }
        $app->register(new Provider\TwigServiceProvider());

        return true;
    }


    /**
     * Initialise debug web profiler
     *
     * @return bool
     */
    protected function initWebProfiler()
    {
        $app = $this;

        if (!$app['debug']) {
            return true;
        }

        $app->init('Controllers');
        $app->init('Twig');

        $app->register(new Provider\WebProfilerServiceProvider());

        return true;
    }

    /**
     * Implement HTTP caching
     *
     * @return bool
     */
    protected function initHttpCache()
    {
        $app = $this;
        $app->register(new Provider\HttpCacheServiceProvider());

        return true;
    }

    /**
     * Initialise sessions
     *
     * @return bool
     */
    protected function initSessions()
    {
        $app = $this;
        $app->register(new Provider\SessionsServiceProvider());

        return true;
    }

    /**
     * Initialise the bootstrap service provider
     *
     * @return bool
     */
    protected function initBootstrap()
    {
        $app = $this;

        $app->init('Twig');
        $app->init('Forms');
        $app->init('Console');
        $app->init('Assetic');

        $app->register(new Provider\BootstrapServiceProvider());

        return true;
    }

    /**
     * Initialise assetic
     *
     * @return bool
     */
    protected function initAssetic()
    {
        $app = $this;
        $app->register(new Provider\AsseticServiceProvider());

        return true;
    }
}
