<?php
/**
 * @copyright   Copyright (c) 2015 BoxUK Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\EventSubscriber;

use JMS\Serializer\EventDispatcher\Event;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\SerializationContext;

abstract class SerializationEventSubscriber implements EventSubscriberInterface
{
    /**
     * @param $group
     * @param Event $event
     * @return bool
     */
    protected function hasSerializationGroup($group, Event $event)
    {
        /** @var SerializationContext $context */
        $context = $event->getContext();
        if (false === $context->attributes->containsKey('groups')) {
            return false;
        }
        if (false === in_array($group, $context->attributes->get('groups')->get())) {
            return false;
        }

        return true;
    }
}
