<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase;

/**
 * Generic Exception for use on application
 *
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase
 */
class Exception extends \Exception
{

}
