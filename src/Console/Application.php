<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Console;

use Knp\Console\Application as BaseApplication;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class Application
 *
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Console
 */
class Application extends BaseApplication
{
    /**
     * {@inheritDoc}
     */
    protected function getDefaultInputDefinition()
    {
        $inputDefinition = parent::getDefaultInputDefinition();

        $app = $this->getSilexApplication();

        $inputDefinition->addOption(
            new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', $app['env'])
        );

        return $inputDefinition;
    }
}
