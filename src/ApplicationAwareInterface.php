<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase
 */
interface ApplicationAwareInterface
{

    /**
     * @param Application $app
     */
    public function setApplication(Application $app);

}
