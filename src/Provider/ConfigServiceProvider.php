<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Igorw\Silex\ConfigServiceProvider as ConfigProvider;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class ConfigServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $configFilesDir    = $app->getConfigDir();
        $genericConfigFile = $configFilesDir . '/config.yml';
        $app->register(new ConfigProvider($genericConfigFile, $app->getConfigReplacements()));

        $environmentConfigFile = $configFilesDir . '/config_' . $app['env'] . '.yml';
        if (is_file($environmentConfigFile)) {
            $app->register(new ConfigProvider($environmentConfigFile, $app->getConfigReplacements()));
        }

        $secretsFile = $configFilesDir . '/secrets.yml';
        if (is_file($secretsFile)) {
            $app->register(new ConfigProvider($secretsFile, $app->getConfigReplacements()));
        }

        // make sure directories from config
        $dir = $app['config']['cache']['cache_dir'];

        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
