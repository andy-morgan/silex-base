<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\ValidatorServiceProvider as ValidatorProvider;
use Silex\ServiceProviderInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Validator\Mapping\ClassMetadataFactory;
use Symfony\Component\Validator\Mapping\Loader\YamlFilesLoader;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class ValidatorServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app->register(new ValidatorProvider());

        $app['validator.mapping.class_metadata_factory'] = $app->share(function ($app) {
            $files = array();

            $finder = new Finder();
            if (isset($app['config']['validators']['paths'])) {
                foreach ($app['config']['validators']['paths'] as $dir) {
                    if (!is_dir($dir)) {
                        continue;
                    }
                    foreach ($finder->files()->in($dir) as $file) {
                        $files[] = $file->getPathname();
                    }
                }
            }

            return new ClassMetadataFactory(new YamlFilesLoader($files));
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
