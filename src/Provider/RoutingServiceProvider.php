<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\ServiceProviderInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RouteCollection;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class RoutingServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app->register(new UrlGeneratorServiceProvider());

        $app['routes'] = $app->extend('routes', function (RouteCollection $routes, Application $app) {

            if (!isset($app['config']['routing']['files'])) {
                return $routes;
            }

            foreach ($app['config']['routing']['files'] as $file) {
                $file = new File($file);
                $loader     = new YamlFileLoader(new FileLocator($file->getPath()));
                $collection = $loader->load($file->getFilename());
                $routes->addCollection($collection);
            }

            return $routes;
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
