<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Assetic\Asset\AssetCache;
use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Cache\FilesystemCache;
use Assetic\Filter\CssEmbedFilter;
use Assetic\Filter\LessFilter;
use Assetic\Filter\Yui\CssCompressorFilter;
use Assetic\Filter\Yui\JsCompressorFilter;
use Knp\Console\ConsoleEvent;
use Knp\Console\ConsoleEvents;
use Silex\Application;
use SilexAssetic\AsseticServiceProvider as AsseticProvider;
use Silex\ServiceProviderInterface;
use Symfony\Component\Finder\Finder;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class AsseticServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app->register(new AsseticProvider(), array(
            'assetic.path_to_web' => $app->getRootDir() . '/web',
            'assetic.options'     => array(
                'debug'            => $app['debug'],
                'auto_dump_assets' => $app['config']['assetic']['auto_dump'],
            )
        ));

        $app['assetic.filter_manager'] = $app->share(
            $app->extend('assetic.filter_manager', function ($fm, $app) {
                $fm->set('less', new LessFilter(
                    $app['config']['assetic']['node_path'],
                    $app['config']['assetic']['node_modules_paths']
                ));
                $cssEmbedJar = $app->getRootDir() . '/bin/cssembed-0.4.5.jar';
                $fm->set('cssembed', new CssEmbedFilter($cssEmbedJar));
                $yuiCompressorJar = $app->getRootDir() . '/bin/yuicompressor-2.4.7.jar';
                $fm->set('cssyuicompressor', new CssCompressorFilter($yuiCompressorJar));
                $fm->set('jsyuicompressor', new JsCompressorFilter($yuiCompressorJar));

                return $fm;
            })
        );

        $app['assetic.asset_manager'] = $app->share(
            $app->extend('assetic.asset_manager', function ($am, $app) {

                $cssFilters = array(
                    $app['assetic.filter_manager']->get('cssembed'),
                    $app['assetic.filter_manager']->get('less')
                );
                $jsFilters  = array();

                if (!$app['debug']) {
                    $cssFilters[] = $app['assetic.filter_manager']->get('cssyuicompressor');
                    $jsFilters[]  = $app['assetic.filter_manager']->get('jsyuicompressor');
                }

                $cssCollection = new AssetCollection(array(), $cssFilters);
                $app['assetic.collections.web_styles'] = $cssCollection;

                $jsCollection  = new AssetCollection(array(), $jsFilters);
                $app['assetic.collections.web_scripts'] = $jsCollection;

                $fileTypes = array('less' => 'cssCollection', 'css' => 'cssCollection', 'js' => 'cssCollection');

                $finder = new Finder();
                foreach ($app['config']['assetic']['paths'] as $dir) {
                    foreach($fileTypes as $type => $collectionVar) {
                        $dir = $dir . '/' . $type;
                        if (is_dir($dir)) {
                            foreach ($finder->files()->in($dir) as $file) {
                                $$collectionVar->add(new FileAsset($file->getPathName()));
                            }
                        }
                    }
                }

                $asseticCache = new FilesystemCache($app['config']['cache']['cache_dir'] . '/assetic');

                $am->set('web_styles', new AssetCache($cssCollection, $asseticCache));
                $am->get('web_styles')->setTargetPath('/css/all.css');

                $am->set('web_scripts', new AssetCache($jsCollection, $asseticCache));
                $am->get('web_scripts')->setTargetPath('/js/all.js');

                return $am;
            })
        );

        $app['dispatcher']->addListener(ConsoleEvents::INIT, function (ConsoleEvent $event) {
            /** @var \SilexBase\Console\Application $app */
            $consoleApp = $event->getApplication();

            $consoleApp->add(new \SilexBase\Command\AsseticDumpCommand());
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
