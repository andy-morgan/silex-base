<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\FormServiceProvider as FormProvider;
use Silex\ServiceProviderInterface;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class FormServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app->register(new FormProvider());

        if (isset($app['doctrine'])) {
            $app['form.extensions'] = $app->share($app->extend('form.extensions', function ($extensions, $app) {
                $extensions[] = new DoctrineOrmExtension($app['doctrine']);

                return $extensions;
            }));
        }
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
