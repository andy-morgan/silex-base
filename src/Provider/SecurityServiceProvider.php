<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\SecurityServiceProvider as SecurityProvider;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class SecurityServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app->register(new SecurityProvider(), array(
            'security.firewalls' => array(
                'main' => array(
                    'pattern'   => '^/',
                    'form'      => array(
                        'use_referer'              => true,
                        'post_only'                => true,
                        'require_previous_session' => true,
                    ),
                    'anonymous' => true,
                ),
            ),
            'security.role_hierarchy' => array(
                'ROLE_SUPER_ADMIN' => array('ROLE_ADMIN'),
                'ROLE_ADMIN'       => array('ROLE_USER'),
            ),
            'security.access_rules' => array(
                array('^/login$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
                array('^/auth', 'ROLE_USER'),
            )
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
