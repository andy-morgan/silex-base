<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Saxulum\SaxulumWebProfiler\Provider\SaxulumWebProfilerProvider;
use Silex\Application;
use Silex\Provider\WebProfilerServiceProvider as WebProfilerProvider;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class WebProfilerServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        if (isset($app['config']['web_profiler']['enabled']) && $app['config']['web_profiler']['enabled'] === false) {
            return;
        }

        $app->register(
            new WebProfilerProvider(),
            array(
                'profiler.cache_dir'    => $app['config']['cache']['cache_dir'] . '/profiler',
                'profiler.mount_prefix' => '/_profiler',
                'web_profiler.debug_toolbar.enable' =>
                    isset($app['config']['web_profiler']['toolbar']) ? $app['config']['web_profiler']['toolbar'] : true
            )
        );

        if (class_exists('Saxulum\SaxulumWebProfiler\Provider\SaxulumWebProfilerProvider')) {
            $app->register(new SaxulumWebProfilerProvider());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
