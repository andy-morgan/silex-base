<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\SerializerBuilder;
use Silex\Application;
use Silex\ServiceProviderInterface;
use SilexBase\Service\SerializerService;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class JMSSerializerServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app['serializer'] = $app->share(function($app) {
            $serializerBuilder = SerializerBuilder::create()
                ->setDebug(isset($app['serializer.debug']) ? (bool) $app['serializer.debug'] : $app['debug']);

            // set cache dir
            if (isset($app['serializer.cache_dir']) && !is_null($app['serializer.cache_dir'])) {
                if (!is_dir($app['serializer.cache_dir'])) {
                    mkdir($app['serializer.cache_dir'], 0755, true);
                }
                $serializerBuilder->setCacheDir($app['serializer.cache_dir']);
            }

            // add metadata paths
            if (isset($app['serializer.metadata']) && !empty($app['serializer.metadata'])) {
                foreach ($app['serializer.metadata'] as $metadata) {
                    if (!array_key_exists('path', $metadata) || !is_dir($metadata['path'])) {
                        continue;
                    }
                    $serializerBuilder->addMetadataDir($metadata['path'], $metadata['namespace']);
                }
            }

            if (isset($app['serializer.event_subscribers']) && !empty($app['serializer.event_subscribers'])) {
                $serializerBuilder->configureListeners(function (EventDispatcher $dispatcher) use ($app) {
                    foreach ($app['serializer.event_subscribers'] as $subscriber) {
                        $dispatcher->addSubscriber($subscriber);
                    }
                });
            }

            return $serializerBuilder->build();
        });

        $app['serializer.service'] = $app->share(function($app) {
            return new SerializerService($app['serializer']);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
