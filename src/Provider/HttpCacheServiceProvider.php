<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\HttpCacheServiceProvider as HttpCacheProvider;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class HttpCacheServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app->register(
            new HttpCacheProvider,
            array(
                'http_cache.cache_dir' => $app['config']['cache']['cache_dir'] . '/http',
                'http_cache.esi'       => null,
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
