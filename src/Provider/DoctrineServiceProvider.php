<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Gedmo\Blameable\BlameableListener;
use Knp\Console\ConsoleEvent;
use Knp\Console\ConsoleEvents;
use Saxulum\DoctrineOrmManagerRegistry\Silex\Provider\DoctrineOrmManagerRegistryProvider;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider as DoctrineProvider;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class DoctrineServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $params = array();
        foreach ($app['config']['database']['options'] as $key => $value) {
            $params['db.options'][$key] = $value;
        }

        // if using a sqlite db, make the db dir if it doesn't exist
        if (isset($params['db.options']['driver']) &&
            $params['db.options']['driver'] == "pdo_sqlite" &&
            $params['db.options']['path'] != ':memory:'
        ) {
            $dir = dirname($params['db.options']['path']);

            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }
        }

        $app->register(new DoctrineProvider(), $params);

        $options = array(
            'orm.proxies_dir' => $app['config']['cache']['cache_dir'] . '/doctrine/proxies',
            'orm.em.options' => isset($app['config']['database']['mappings'])
                ? array('mappings' => $app['config']['database']['mappings'])
                : array()
        );

        foreach (array('datetime', 'numeric', 'string') as $type) {
            if (isset($app['config']['database']['functions'][$type])) {
                $options['orm.custom.functions.' . $type] = $app['config']['database']['functions'][$type];
            }
        }

        $app->register(new DoctrineOrmServiceProvider, $options);

        if (isset($app['config']['database']['event_subscribers'])) {
            foreach($app['config']['database']['event_subscribers'] as $class) {
                if (!class_exists($class)) {
                    throw new \LogicException("No class $class found");
                }
                $app['db.event_manager']->addEventSubscriber(new $class());
            }
        }

        $app->register(new DoctrineOrmManagerRegistryProvider());

        $app->on(KernelEvents::REQUEST, function() use ($app) {
            /** @var \Doctrine\Common\EventManager $eventManager */
            $eventManager = $app['db.event_manager'];

            foreach ($eventManager->getListeners() as $type => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof BlameableListener) {
                        $listener->setUserValue($app->getCurrentUser());
                    }
                }
            }
        });

        $app['dispatcher']->addListener(ConsoleEvents::INIT, function (ConsoleEvent $event) {
            /** @var \SilexBase\Console\Application $app */
            $consoleApp = $event->getApplication();

            // DBAL commands
            $consoleApp->add(new \Doctrine\DBAL\Tools\Console\Command\RunSqlCommand());
            $consoleApp->add(new \Doctrine\DBAL\Tools\Console\Command\ImportCommand());

            // ORM commands
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\EnsureProductionSettingsCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\GenerateRepositoriesCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\GenerateEntitiesCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\RunDqlCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand());
            $consoleApp->add(new \Doctrine\ORM\Tools\Console\Command\InfoCommand());

            $consoleApp->add(new \SilexBase\Command\OrmLoadFixturesCommand());
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
