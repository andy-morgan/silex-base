<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use CHH\Silex\CacheServiceProvider as CacheProvider;
use Knp\Console\ConsoleEvent;
use Knp\Console\ConsoleEvents;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class CacheServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        // make sure any cache dir is created
        foreach ($app['config']['cache']['options'] as $cache) {
            if ($cache['driver'] == 'filesystem' && !is_dir($cache['directory'])) {
                mkdir($cache['directory'], 0755, true);
            }
        }

        $app->register(new CacheProvider(), array('cache.options' => $app['config']['cache']['options']));

        $app['dispatcher']->addListener(ConsoleEvents::INIT, function (ConsoleEvent $event) {
            /** @var \SilexBase\Console\Application $app */
            $consoleApp = $event->getApplication();

            $consoleApp->add(new \SilexBase\Command\CacheClearCommand());
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
