<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\TranslationServiceProvider as TranslationProvider;
use Silex\ServiceProviderInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Translation\Loader\YamlFileLoader;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class TranslationServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app['langs'] = array('en');

        $app->register(
            new TranslationProvider,
            array(
                'translator.messages' => array(),
                'locale_fallbacks' => array('en'),
            )
        );

        $app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
            /** @var $translator \Symfony\Component\Translation\Translator */
            $translator->addLoader('yaml', new YamlFileLoader());
            $finder = new Finder();

            if (isset($app['config']['translations']['paths'])) {
                foreach ($app['config']['translations']['paths'] as $dir) {
                    foreach($app['langs'] as $lang) {
                        $langDir = $dir . '/' . $lang;
                        if (is_dir($langDir)) {
                            foreach ($finder->files()->in($langDir) as $file) {
                                $translator->addResource('yaml', $file, $lang, $file->getBasename('.yml'));
                            }
                        }
                    }
                }
            }

            return $translator;
        }));
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
