<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Assetic\Asset\FileAsset;
use Assetic\AssetManager;
use Braincrafted\Bootstrap\BootstrapServiceProvider as BootstrapProvider;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class BootstrapServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $options = array();
        if (isset($app['config']['braincrafted_bootstrap.customize'])) {
            $options['braincrafted_bootstrap.customize'] = $app['config']['braincrafted_bootstrap.customize'];
        }
        $app->register(new BootstrapProvider(), $options);

        $app->extend('assetic.asset_manager', function (AssetManager $am, $app) {
            $app['assetic.collections.web_scripts']->add(
                new FileAsset(
                    $app->getVendorDir()
                    . '/braincrafted/bootstrap-service-provider/Resources/js/bc-bootstrap-collection.js'
                )
            );

            return $am;
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
