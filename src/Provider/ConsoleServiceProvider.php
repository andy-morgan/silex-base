<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Knp\Console\ConsoleEvent;
use Knp\Console\ConsoleEvents;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Console\Helper\HelperSet;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class ConsoleServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app['console'] = $app->share(function () use ($app) {
            $application = new \SilexBase\Console\Application(
                $app,
                realpath(__DIR__ . '/../../../'),
                'Console',
                '1.0.0'
            );

            if (isset($app['config']['database'])) {
                $application->setHelperSet(new HelperSet(array(
                    'db' => new ConnectionHelper($app['orm.em']->getConnection()),
                    'em' => new EntityManagerHelper($app['orm.em'])
                )));
            }

            $app['dispatcher']->dispatch(ConsoleEvents::INIT, new ConsoleEvent($application));

            return $application;
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
