<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class ServicesServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app['security.encoder_factory'] = $app->share(function ($app) {
            return new EncoderFactory(array(
                'Symfony\Component\Security\Core\User\UserInterface' => $app['security.encoder.digest'],
            ));
        });

        $app['security.encoder.digest'] = $app->share(function ($app) {
            return new MessageDigestPasswordEncoder();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
