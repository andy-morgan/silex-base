<?php
/**
 * @copyright   Copyright (c) 2015 BoxUK Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use SilexBase\EventListener\ApiExceptionListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @package SilexBase\Provider
 */
class JsonRequestServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $this->handleJsonRequestBody($app);
        $this->handleHttpExceptions($app);
    }

    /**
     * @param Application $app
     */
    private function handleJsonRequestBody(Application $app)
    {
        $app->before(function (Request $request) {
            if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $data = json_decode($request->getContent(), true);
                $request->request->replace(is_array($data) ? $data : array());
            }
        });
    }

    /**
     * @param Application $app
     */
    private function handleHttpExceptions(Application $app)
    {
        $app->on(
            KernelEvents::EXCEPTION,
            function (GetResponseForExceptionEvent $event) use ($app) {
                $exceptionListener = new ApiExceptionListener($app['debug']);
                $exceptionListener->onCoreException($event);
            },
            Application::LATE_EVENT
        );
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
