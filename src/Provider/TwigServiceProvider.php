<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\TwigServiceProvider as TwigProvider;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class TwigServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $options = array();

        if (!$app->isDevEnv()) {
            $options['cache'] = $app['config']['cache']['cache_dir'] . '/twig';
        }

        $app->register(
            new TwigProvider(),
            array(
                'twig.path'           =>
                    isset($app['config']['views']['paths']) ? $app['config']['views']['paths'] : array(),
                'twig.options'        => $options,
                'twig.form.templates' => array('Form/bootstrap.html.twig'),
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
