<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Provider;

use Silex\Application;
use Silex\Provider\SessionServiceProvider as SessionProvider;
use Silex\ServiceProviderInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Provider
 */
class SessionsServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $params = array();
        foreach ($app['config']['sessions'] as $key => $value) {
            $params['session.' . $key] = $value;
        }

        if (!isset($params['session.storage.handler']) && isset($params['session.storage.save_path'])) {
            if (!is_dir($params['session.storage.save_path'])) {
                mkdir($params['session.storage.save_path'], 0755, true);
            }
        }

        $app->register(new SessionProvider(), $params);
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
