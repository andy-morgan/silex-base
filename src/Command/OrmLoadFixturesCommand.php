<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\Command;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use InvalidArgumentException;
use Knp\Command\Command as BaseCommand;
use SilexBase\Application;
use SilexBase\DataFixtures\Loader;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase\Command
 */
class OrmLoadFixturesCommand extends BaseCommand
{
    /**
     * @see Symfony\Bundle\FrameworkBundle\Command\Command
     */
    public function configure()
    {
        $this
            ->setName('orm:fixtures:load')
            ->addOption(
                'fixtures',
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'The directory or file to load data fixtures from.'
            )
            ->addOption(
                'append',
                null,
                InputOption::VALUE_NONE,
                'Append the data fixtures instead of deleting all data from the database first.'
            )
            ->addOption('truncate', null, InputOption::VALUE_NONE, 'Truncates the old db before running')
            ->setDescription('Loads fixtures')
            ->setHelp(<<<EOT
The <info>%command.name%</info> command command loads data fixtures

You can specify the path to fixtures with the <info>--fixtures</info> option:

  <info>%command.name% --fixtures=/path/to/fixtures1 --fixtures=/path/to/fixtures2</info>

If you want to append the fixtures instead of flushing the database first you can use the <info>--append</info> option:

  <info>%command.name%  --fixtures=/path/to/fixtures1 --append</info>

By default Doctrine Data Fixtures uses DELETE statements to drop the existing rows from
the database. If you want to use a TRUNCATE statement instead you can use the <info>--truncate</info> flag:

  <info>%command.name%  --fixtures=/path/to/fixtures1 --truncate</info>
EOT
            )
        ;
    }

    /**
     * @see Command
     *
     * @throws \InvalidArgumentException When the target directory does not exist
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Application $app */
        $app = $this->getSilexApplication();

        $em = $app['orm.em'];

        $dirOrFile = $input->getOption('fixtures');
        $paths = array();
        if ($dirOrFile) {
            $paths = is_array($dirOrFile) ? $dirOrFile : array($dirOrFile);
        }

        $loader = new Loader($app);
        foreach ($paths as $dir) {
            if (is_dir($dir)) {
                $loader->loadFromDirectory($dir);
            }
        }

        $fixtures = $loader->getFixtures();
        if (!$fixtures) {
            throw new InvalidArgumentException(
                sprintf('Could not find any fixtures to load in: %s',  "\n\n- ".implode("\n- ", $paths))
            );
        }

        $purger = new ORMPurger($em);
        $purger->setPurgeMode(
            ($input->getOption('truncate')) ? ORMPurger::PURGE_MODE_TRUNCATE : ORMPurger::PURGE_MODE_DELETE
        );

        $executor = new ORMExecutor($em, $purger);
        $executor->setLogger(function ($message) use ($output) {
            $output->writeln(sprintf('  <comment>></comment> <info>%s</info>', $message));
        });

        $executor->execute($fixtures, $input->getOption('append'));
    }
}
