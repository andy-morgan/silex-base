<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase;

use Silex\Application as SilexApplication;
use SilexBase\Provider as Provider;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Main application of the site
 *
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase
 */
class Application extends SilexApplication
{
    /**
     * @var array
     */
    protected $initialised = array();

    /**
     * {@inheritDoc}
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        if (!isset($this['env'])) {
            $this['env'] = 'prod';
        }

        $this->init('Config');
        $this->init('Logging');
        $this->init('Routing');
        $this->init('Cache');
        $this->init('Db');
        $this->init('Services');
        $this->init('Forms');
        $this->init('Console');
    }

    /**
     * @param string $service
     */
    public function init($service)
    {
        if (in_array($service, $this->initialised)) {
            return;
        }

        if ($this->{'init' . $service}()) {
            $this->initialised[] = $service;
        }
    }

    /**
     * @return string
     */
    public function getConfigDir()
    {
        return $this->getRootDir() . '/app/config';
    }

    /**
     * @return string
     */
    public function getRootDir()
    {
        return realpath(__DIR__ . '/../../../../');
    }

    /**
     * @return string
     */
    public function getVendorDir()
    {
        return $this->getRootDir() . '/vendor';
    }

    /**
     * @return UserInterface/null
     */
    public function getCurrentUser()
    {
        $app = $this;

        if (!array_key_exists('security', $this->values)) {
            throw new \LogicException('Cannot get user without first initialising Security');
        }

        $token = $app['security']->getToken();
        if (null !== $token) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isDevEnv()
    {
        return substr($this['env'], 0, 3) == 'dev';
    }

    /**
     * Setup any app config
     *
     * @return bool
     */
    protected function initConfig()
    {
        $app = $this;
        $app->register(new Provider\ConfigServiceProvider());

        return true;
    }

    /**
     * Implement bucket cache
     *
     * @return bool
     */
    protected function initCache()
    {
        $app = $this;
        $app->register(new Provider\CacheServiceProvider());

        return true;
    }

    /**
     * Setup logs in the application
     *
     * @return bool
     */
    protected function initLogging()
    {
        $app = $this;
        $app->register(new Provider\LoggingServiceProvider());

        return true;
    }

    /**
     * Initialise all routes in the application
     *
     * @return bool
     */
    protected function initRouting()
    {
        $app = $this;
        $app->register(new Provider\RoutingServiceProvider());

        return true;
    }

    /**
     * Initialise the database connection
     *
     * @return bool
     */
    protected function initDb()
    {
        $app = $this;

        $app->init('Config');

        if (isset($app['config']['database'])) {
            $app->register(new Provider\DoctrineServiceProvider());
        }

        return true;
    }

    /**
     * Initialise the application services
     *
     * @return bool
     */
    protected function initServices()
    {
        $app = $this;
        $app->register(new Provider\ServicesServiceProvider());

        return true;
    }

    /**
     * Controllers as services to be initialised here
     *
     * @return bool
     */
    protected function initControllers()
    {
        $app = $this;
        $app->register(new Provider\ControllersServiceProvider());

        return true;
    }

    /**
     * Initialise the console.
     *
     * @return bool
     */
    protected function initConsole()
    {
        $app = $this;
        $app->register(new Provider\ConsoleServiceProvider());

        return true;
    }

    /**
     * Initialise the forms, and also its dependencies
     *
     * @return bool
     */
    protected function initForms()
    {
        $app = $this;

        $app->init('Validator');
        $app->init('Translations');

        $app->register(new Provider\FormServiceProvider());

        return true;
    }

    /**
     * Initialise validation
     *
     * @return bool
     */
    protected function initValidator()
    {
        $app = $this;
        $app->register(new Provider\ValidatorServiceProvider());

        return true;
    }

    /**
     * Initialise translations
     *
     * @return bool
     */
    protected function initTranslations()
    {
        $app = $this;
        $app->register(new Provider\TranslationServiceProvider());

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet($id)
    {
        if (!array_key_exists($id, $this->values)) {
            throw new \InvalidArgumentException(sprintf('Identifier "%s" is not defined.', $id));
        }

        $isFactory = is_object($this->values[$id]) && method_exists($this->values[$id], '__invoke');

        if ($isFactory) {
            return $this->values[$id]($this);
        }

        $value = $this->values[$id];
        if (is_string($value) && $id != 'env') {
            $value = $this->doConfigReplacements($value);
        }

        if (is_array($value)) {
            // @todo: make this more recursive?
            foreach ($value as $k => $v) {
                if (is_string($v)) {
                    $value[$k] = $this->doConfigReplacements($v);
                }
            }
        }

        return $value;
    }

    /**
     * @param string $value
     * @return mixed
     * @throws \LogicException
     */
    public function doConfigReplacements($value)
    {
        if (!is_string($value)) {
            throw new \LogicException('Value must be a string');
        }

        foreach ($this->getConfigReplacements() as $search => $replace) {
            $value = str_replace('%' . $search . '%', $replace, $value);
        }

        return $value;
    }

    /**
     * @return array
     */
    public function getConfigReplacements()
    {
        $app = $this;

        $replacements = array(
            'root_dir' => $this->getRootDir(),
            'app_dir'  => $this->getRootDir() . '/app',
            'src_dir'  => $this->getRootDir() . '/src',
            'web_dir'  => $this->getRootDir() . '/web',
            'env'      => $app['env'],
        );

        return $replacements;
    }
}
