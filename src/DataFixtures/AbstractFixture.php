<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture as DoctrineAbstractFixture;
use SilexBase\Application;
use SilexBase\ApplicationAwareInterface;

/**
 * Base application of an API
 *
 * @author      Andy Morgan <andy@andy-morgan.co.uk>
 * @package     SilexBase
 */
abstract class AbstractFixture extends DoctrineAbstractFixture implements ApplicationAwareInterface
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @param Application $app
     */
    public function setApplication(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param Object $entity
     * @param string $property
     * @param mixed $value
     */
    protected function setPropertyUsingReflection($entity, $property, $value)
    {
        $refObject = new \ReflectionClass($entity);
        $property = $refObject->getProperty($property);
        $property->setAccessible(true);
        $property->setValue($entity, $value);
    }
}
