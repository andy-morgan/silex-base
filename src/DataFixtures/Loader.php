<?php
/**
 * @copyright   Copyright (c) 2015 Andy Morgan
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SilexBase\DataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use SilexBase\Application;
use SilexBase\ApplicationAwareInterface;

class Loader extends \Doctrine\Common\DataFixtures\Loader
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * {@inheritDoc)
     */
    public function addFixture(FixtureInterface $fixture)
    {
        parent::addFixture($fixture);

        if ($fixture instanceof ApplicationAwareInterface) {
            $fixture->setApplication($this->app);
        }
    }
}
