# README #

### What is this repository for? ###

This is a(n opinionated) base skeleton for a silex application

### How do I get it on my project? ###

```
#!bash
composer require andy-morgan/silex-base
```